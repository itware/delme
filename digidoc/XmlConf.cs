//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace digidoc {

public class XmlConf : Conf {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal XmlConf(global::System.IntPtr cPtr, bool cMemoryOwn) : base(digidocPINVOKE.XmlConf_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(XmlConf obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  protected override void Dispose(bool disposing) {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          digidocPINVOKE.delete_XmlConf(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      base.Dispose(disposing);
    }
  }

  public XmlConf(string path, string schema) : this(digidocPINVOKE.new_XmlConf__SWIG_0(path, schema), true) {
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public XmlConf(string path) : this(digidocPINVOKE.new_XmlConf__SWIG_1(path), true) {
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public XmlConf() : this(digidocPINVOKE.new_XmlConf__SWIG_2(), true) {
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public new static XmlConf instance() {
    global::System.IntPtr cPtr = digidocPINVOKE.XmlConf_instance();
    XmlConf ret = (cPtr == global::System.IntPtr.Zero) ? null : new XmlConf(cPtr, false);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override int logLevel() {
    int ret = digidocPINVOKE.XmlConf_logLevel(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string logFile() {
    string ret = digidocPINVOKE.XmlConf_logFile(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string PKCS11Driver() {
    string ret = digidocPINVOKE.XmlConf_PKCS11Driver(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string proxyHost() {
    string ret = digidocPINVOKE.XmlConf_proxyHost(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string proxyPort() {
    string ret = digidocPINVOKE.XmlConf_proxyPort(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string proxyUser() {
    string ret = digidocPINVOKE.XmlConf_proxyUser(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string proxyPass() {
    string ret = digidocPINVOKE.XmlConf_proxyPass(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool proxyForceSSL() {
    bool ret = digidocPINVOKE.XmlConf_proxyForceSSL(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool proxyTunnelSSL() {
    bool ret = digidocPINVOKE.XmlConf_proxyTunnelSSL(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string digestUri() {
    string ret = digidocPINVOKE.XmlConf_digestUri(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string signatureDigestUri() {
    string ret = digidocPINVOKE.XmlConf_signatureDigestUri(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string ocsp(string issuer) {
    string ret = digidocPINVOKE.XmlConf_ocsp(swigCPtr, issuer);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string TSUrl() {
    string ret = digidocPINVOKE.XmlConf_TSUrl(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string verifyServiceUri() {
    string ret = digidocPINVOKE.XmlConf_verifyServiceUri(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string PKCS12Cert() {
    string ret = digidocPINVOKE.XmlConf_PKCS12Cert(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string PKCS12Pass() {
    string ret = digidocPINVOKE.XmlConf_PKCS12Pass(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool PKCS12Disable() {
    bool ret = digidocPINVOKE.XmlConf_PKCS12Disable(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool TSLAutoUpdate() {
    bool ret = digidocPINVOKE.XmlConf_TSLAutoUpdate(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string TSLCache() {
    string ret = digidocPINVOKE.XmlConf_TSLCache(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool TSLOnlineDigest() {
    bool ret = digidocPINVOKE.XmlConf_TSLOnlineDigest(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override int TSLTimeOut() {
    int ret = digidocPINVOKE.XmlConf_TSLTimeOut(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual void setProxyHost(string host) {
    digidocPINVOKE.XmlConf_setProxyHost(swigCPtr, host);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setProxyPort(string port) {
    digidocPINVOKE.XmlConf_setProxyPort(swigCPtr, port);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setProxyUser(string user) {
    digidocPINVOKE.XmlConf_setProxyUser(swigCPtr, user);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setProxyPass(string pass) {
    digidocPINVOKE.XmlConf_setProxyPass(swigCPtr, pass);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setProxyTunnelSSL(bool enable) {
    digidocPINVOKE.XmlConf_setProxyTunnelSSL(swigCPtr, enable);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setPKCS12Cert(string cert) {
    digidocPINVOKE.XmlConf_setPKCS12Cert(swigCPtr, cert);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setPKCS12Pass(string pass) {
    digidocPINVOKE.XmlConf_setPKCS12Pass(swigCPtr, pass);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setPKCS12Disable(bool disable) {
    digidocPINVOKE.XmlConf_setPKCS12Disable(swigCPtr, disable);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setTSLOnlineDigest(bool enable) {
    digidocPINVOKE.XmlConf_setTSLOnlineDigest(swigCPtr, enable);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setTSLTimeOut(int timeOut) {
    digidocPINVOKE.XmlConf_setTSLTimeOut(swigCPtr, timeOut);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setTSUrl(string url) {
    digidocPINVOKE.XmlConf_setTSUrl(swigCPtr, url);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

}

}
