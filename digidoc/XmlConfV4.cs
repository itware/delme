//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace digidoc {

public class XmlConfV4 : ConfV4 {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal XmlConfV4(global::System.IntPtr cPtr, bool cMemoryOwn) : base(digidocPINVOKE.XmlConfV4_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(XmlConfV4 obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  protected override void Dispose(bool disposing) {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          digidocPINVOKE.delete_XmlConfV4(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      base.Dispose(disposing);
    }
  }

  public XmlConfV4(string path, string schema) : this(digidocPINVOKE.new_XmlConfV4__SWIG_0(path, schema), true) {
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public XmlConfV4(string path) : this(digidocPINVOKE.new_XmlConfV4__SWIG_1(path), true) {
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public XmlConfV4() : this(digidocPINVOKE.new_XmlConfV4__SWIG_2(), true) {
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public new static XmlConfV4 instance() {
    global::System.IntPtr cPtr = digidocPINVOKE.XmlConfV4_instance();
    XmlConfV4 ret = (cPtr == global::System.IntPtr.Zero) ? null : new XmlConfV4(cPtr, false);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override int logLevel() {
    int ret = digidocPINVOKE.XmlConfV4_logLevel(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string logFile() {
    string ret = digidocPINVOKE.XmlConfV4_logFile(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string PKCS11Driver() {
    string ret = digidocPINVOKE.XmlConfV4_PKCS11Driver(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string proxyHost() {
    string ret = digidocPINVOKE.XmlConfV4_proxyHost(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string proxyPort() {
    string ret = digidocPINVOKE.XmlConfV4_proxyPort(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string proxyUser() {
    string ret = digidocPINVOKE.XmlConfV4_proxyUser(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string proxyPass() {
    string ret = digidocPINVOKE.XmlConfV4_proxyPass(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool proxyForceSSL() {
    bool ret = digidocPINVOKE.XmlConfV4_proxyForceSSL(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool proxyTunnelSSL() {
    bool ret = digidocPINVOKE.XmlConfV4_proxyTunnelSSL(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string digestUri() {
    string ret = digidocPINVOKE.XmlConfV4_digestUri(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string signatureDigestUri() {
    string ret = digidocPINVOKE.XmlConfV4_signatureDigestUri(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string ocsp(string issuer) {
    string ret = digidocPINVOKE.XmlConfV4_ocsp(swigCPtr, issuer);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string TSUrl() {
    string ret = digidocPINVOKE.XmlConfV4_TSUrl(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string verifyServiceUri() {
    string ret = digidocPINVOKE.XmlConfV4_verifyServiceUri(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string PKCS12Cert() {
    string ret = digidocPINVOKE.XmlConfV4_PKCS12Cert(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string PKCS12Pass() {
    string ret = digidocPINVOKE.XmlConfV4_PKCS12Pass(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool PKCS12Disable() {
    bool ret = digidocPINVOKE.XmlConfV4_PKCS12Disable(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool TSLAutoUpdate() {
    bool ret = digidocPINVOKE.XmlConfV4_TSLAutoUpdate(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override string TSLCache() {
    string ret = digidocPINVOKE.XmlConfV4_TSLCache(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override bool TSLOnlineDigest() {
    bool ret = digidocPINVOKE.XmlConfV4_TSLOnlineDigest(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public override int TSLTimeOut() {
    int ret = digidocPINVOKE.XmlConfV4_TSLTimeOut(swigCPtr);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual void setProxyHost(string host) {
    digidocPINVOKE.XmlConfV4_setProxyHost(swigCPtr, host);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setProxyPort(string port) {
    digidocPINVOKE.XmlConfV4_setProxyPort(swigCPtr, port);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setProxyUser(string user) {
    digidocPINVOKE.XmlConfV4_setProxyUser(swigCPtr, user);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setProxyPass(string pass) {
    digidocPINVOKE.XmlConfV4_setProxyPass(swigCPtr, pass);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setProxyTunnelSSL(bool enable) {
    digidocPINVOKE.XmlConfV4_setProxyTunnelSSL(swigCPtr, enable);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setPKCS12Cert(string cert) {
    digidocPINVOKE.XmlConfV4_setPKCS12Cert(swigCPtr, cert);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setPKCS12Pass(string pass) {
    digidocPINVOKE.XmlConfV4_setPKCS12Pass(swigCPtr, pass);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setPKCS12Disable(bool disable) {
    digidocPINVOKE.XmlConfV4_setPKCS12Disable(swigCPtr, disable);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setTSLOnlineDigest(bool enable) {
    digidocPINVOKE.XmlConfV4_setTSLOnlineDigest(swigCPtr, enable);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setTSLTimeOut(int timeOut) {
    digidocPINVOKE.XmlConfV4_setTSLTimeOut(swigCPtr, timeOut);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual void setTSUrl(string url) {
    digidocPINVOKE.XmlConfV4_setTSUrl(swigCPtr, url);
    if (digidocPINVOKE.SWIGPendingException.Pending) throw digidocPINVOKE.SWIGPendingException.Retrieve();
  }

}

}
