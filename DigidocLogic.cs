﻿using System.Security.Cryptography.X509Certificates;
using digidoc;

namespace DelMe
{
    internal class DigidocLogic
    {
        public static void Verify(string file)
        {
            Console.WriteLine("Verifing: " + file);

            digidoc.digidoc.initialize();
            try
            {
                Console.WriteLine("Opening file: " + file);
                Container b = Container.open(file);

                Console.WriteLine("Files:");
                foreach (DataFile d in b.dataFiles())
                {
                    Console.WriteLine(" {0} - {1}", d.fileName(), d.mediaType());
                }
                Console.WriteLine();

                Console.WriteLine("Signatures:");
                foreach (Signature s in b.signatures())
                {
                    Console.WriteLine("Address: {0} {1} {2} {3}", s.city(), s.countryName(), s.stateOrProvince(), s.postalCode());

                    Console.Write("Role:");
                    foreach (string role in s.signerRoles())
                    {
                        Console.Write(" " + role);
                    }
                    Console.WriteLine();

                    Console.WriteLine("Time: " + s.trustedSigningTime());
                    Console.WriteLine("Cert: " + new X509Certificate2(s.signingCertificateDer()).Subject);

                    s.validate();
                    Console.WriteLine("Signature is valid");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Signature is invalid");
                Console.WriteLine(e.Message);
            }
            digidoc.digidoc.terminate();
        }
    }
}
